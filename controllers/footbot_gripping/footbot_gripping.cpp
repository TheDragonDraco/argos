/* Include the controller definition */
#include "footbot_gripping.h"


/****************************************/
/****************************************/

CFootBotGripping::CFootBotGripping() :
   m_pcWheels(NULL),
   m_pcGripper(NULL),
   m_unCounter(0), 
   m_pcProximity(NULL),
   m_cAlpha(0.5f),
   m_fDelta(.05f),
   m_fWheelVelocity(1.5f),
   m_cGoStraightAngleRange(-ToRadians(m_cAlpha),
                           ToRadians(m_cAlpha)),
  m_stc()

{}

/****************************************/
/****************************************/

void CFootBotGripping::Init(TConfigurationNode& t_node) {
   /*
    * Get sensor/actuator handles
    *
    * The passed string (ex. "differential_steering") corresponds to the
    * XML tag of the device whose handle we want to have. For a list of
    * allowed values, type at the command prompt:
    *
    * $ argos3 -q actuators
    *
    * to have a list of all the possible actuators, or
    *
    * $ argos3 -q sensors
    *
    * to have a list of all the possible sensors.
    *
    * NOTE: ARGoS creates and initializes actuators and sensors
    * internally, on the basis of the lists provided the configuration
    * file at the <controllers><footbot_gripping><actuators> and
    * <controllers><footbot_gripping><sensors> sections. If you forgot to
    * list a device in the XML and then you request it here, an error
    * occurs.
    */
   m_pcWheels  = GetActuator<CCI_DifferentialSteeringActuator>("differential_steering");
   m_pcGripper = GetActuator<CCI_FootBotGripperActuator>("footbot_gripper"      );
      m_pcProximity = GetSensor  <CCI_FootBotProximitySensor      >("footbot_proximity"    );	
   // m_pcLEDs   = GetActuator<CCI_LEDsActuator>("leds");

  // m_pcCamera = GetSensor  <CCI_ColoredBlobOmnidirectionalCameraSensor>("colored_blob_omnidirectional_camera");
  // m_pcCamera->Enable();	
   
   GetNodeAttributeOrDefault(t_node, "alpha", m_cAlpha, m_cAlpha);
   m_cGoStraightAngleRange.Set(-ToRadians(m_cAlpha), ToRadians(m_cAlpha));
   GetNodeAttributeOrDefault(t_node, "delta", m_fDelta, m_fDelta);
   GetNodeAttributeOrDefault(t_node, "velocity", m_fWheelVelocity, m_fWheelVelocity); 

   m_logfs.open("log.txt",std::ios::out);
   m_logfs << "alpha = "<< m_cAlpha <<" \t delta = "<< m_fDelta <<"\t velocity = "<< m_fWheelVelocity << std::endl;
}

/****************************************/
/****************************************/

void CFootBotGripping::ControlStep() 
{
   	
   switch(m_stc.state)
   {
	case 0:
			Explore(); break;
			
	case 1:
			Turn90(); break;

	case 2:
			TravFor(); break;

	case 3:		
			Turn90(); Turn90(); break;

	case 4:
			TravBack();
   }   


   
}

/****************************************/
/****************************************/

void CFootBotGripping::Reset() {
   /* Reset the counter */
   m_unCounter = 0;
}


void CFootBotGripping::Explore()
{
	  const CCI_FootBotProximitySensor::TReadings& tProxReads = m_pcProximity->GetReadings();
    	     m_logfs << " -------" << std::endl;
	      m_logfs << "Readings: " << tProxReads.size() << std::endl;

  /* Sum them together */
   CVector2 cAccumulator;
   for(size_t i = 0; i < tProxReads.size(); ++i) {
      m_logfs << tProxReads[i].Value<<","<<tProxReads[i].Angle <<"  ";	
      cAccumulator += CVector2(tProxReads[i].Value, tProxReads[i].Angle);
   }

   m_logfs << std::endl;
	
  
   cAccumulator /= tProxReads.size();
   /* If the angle of the vector is small enough and the closest obstacle
    * is far enough, continue going straight, otherwise curve a little
    */
   CRadians cAngle = cAccumulator.Angle();
   m_logfs << "closest length = "<< cAccumulator.Length() << "  angle = "<< cAngle << std::endl; 
  if(m_cGoStraightAngleRange.WithinMinBoundIncludedMaxBoundIncluded(cAngle) &&
      cAccumulator.Length() < m_fDelta ) {
      /* Go straight */
    m_logfs << "Going straight" << std::endl;	
      m_pcWheels->SetLinearVelocity(m_fWheelVelocity, m_fWheelVelocity);
    }
	else m_stc.state = m_stc.TURN90;
	


}

void CFootBotGripping::Turn90()
{
	m_stc.state = m_stc.TURN90;
        m_logfs << "Turning 90";
	if (++m_stc.count < 50) // turn until parallel to obstacle	
       	 m_pcWheels->SetLinearVelocity(m_fWheelVelocity, 0.0f);
	else
	{
		m_stc.count = 0;
		m_stc.state = m_stc.TRAVFOR;
		
	}


}

void CFootBotGripping::TravFor()
{
		
       	 m_pcWheels->SetLinearVelocity(m_fWheelVelocity , m_fWheelVelocity + 0.2);
}

void CFootBotGripping::TravBack()
{
		
       	 m_pcWheels->SetLinearVelocity(-m_fWheelVelocity, -m_fWheelVelocity);
	
}


/****************************************/
/****************************************/

/*
 * This statement notifies ARGoS of the existence of the controller.
 * It binds the class passed as first argument to the string passed as
 * second argument.
 * The string is then usable in the configuration file to refer to this
 * controller.
 * When ARGoS reads that string in the configuration file, it knows which
 * controller class to instantiate.
 * See also the configuration files for an example of how this is used.
 */
REGISTER_CONTROLLER(CFootBotGripping, "footbot_gripping_controller")
